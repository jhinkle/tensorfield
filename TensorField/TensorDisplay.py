import TensorField2D as tf
import numpy as np
import math
import PyCA.Core as ca
import matplotlib.pyplot as plt
plt.ion()

def DispTensorField(TF, cmap='jet', newFig=True, colorbar=True, title=None, Eigenvector=0):
    '''Displays a 2D tensor field by displaying the principal eigenvectors
    as quivers on top of the magnitude of the eigenvectors

    '''
    # do Eig on TF
    evec0 = ca.Field3D(TF.grid(), ca.MEM_DEVICE)
    evec1 = ca.Field3D(TF.grid(), ca.MEM_DEVICE)
    eval0 = ca.Image3D(TF.grid(), ca.MEM_DEVICE)
    eval1 = ca.Image3D(TF.grid(), ca.MEM_DEVICE)

    tf.Eig(evec0, evec1, eval0, eval1, TF)

    if newFig:
        img = plt.figure()
    plt.clf()               # don't be slow!, also get rid of colorbar etc

    evec0.toType(ca.MEM_HOST)
    evec1.toType(ca.MEM_HOST)
    eval0.toType(ca.MEM_HOST)
    eval1.toType(ca.MEM_HOST)

    if Eigenvector == 0:
        vecnp_x = np.squeeze(evec0.asnp()[0].copy())
        vecnp_y = np.squeeze(evec0.asnp()[1].copy())
        valnp = np.squeeze(eval0.asnp().copy())
    else:
        vecnp_x = np.squeeze(evec1.asnp()[0].copy())
        vecnp_y = np.squeeze(evec1.asnp()[1].copy())
        valnp = np.squeeze(eval1.asnp().copy())


    plt.xticks([])
    plt.yticks([])
    plt.axis('tight')
    plt.axis('off')
    plt.axis('equal')


    plt.quiver(vecnp_x, vecnp_y)
    mm = [np.min(valnp), np.max(valnp)]
    if mm[0] == mm[1]:
        vmin = mm[0]
        vmax = mm[0]+1
    else:
        vmin = mm[0]
        vmax = mm[1]
    img = plt.imshow(valnp, vmin=vmin, vmax=vmax, cmap='Oranges')

    plt.colorbar()
    img.set_interpolation('nearest')

    if title is not None:
        plt.title(title)


    plt.show()
    plt.draw()

def DispTensorFieldEllipse(TF, newFig=True, title=None):
    '''Displays a 2D tensor field by displaying the ellipses at each point'''
    from matplotlib.patches import Ellipse

    evec0 = ca.Field3D(TF.grid(), ca.MEM_DEVICE)
    evec1 = ca.Field3D(TF.grid(), ca.MEM_DEVICE)
    eval0 = ca.Image3D(TF.grid(), ca.MEM_DEVICE)
    eval1 = ca.Image3D(TF.grid(), ca.MEM_DEVICE)
    tf.Eig(evec0, evec1, eval0, eval1, TF)
    evec0.toType(ca.MEM_HOST)
    evec1.toType(ca.MEM_HOST)
    eval0.toType(ca.MEM_HOST)
    eval1.toType(ca.MEM_HOST)
    evec0npx = evec0.asnp()[0].copy()
    evec0npy = evec0.asnp()[1].copy()
    eval0np = eval0.asnp().copy()
    eval1np = eval1.asnp().copy()

    # eval0np = np.sqrt(eval0np)
    # eval1np = np.sqrt(eval1np)

    # i want the largest eigenvalues to look about 5x the size of
    # identity, so i take the eigenvalues to the appropriate power to
    # show that.  This creates obvious scaling problems, but at least
    # guarentees that both identity and very large eigenvalues are visible
    mm = ca.MinMax(eval0)
    print "Principal Eigenvalue range", ca.MinMax(eval0)
    if mm[1] > 5.0:
        exp = math.log(5, mm[1])
        eval0np = np.power(eval0np, exp)
        eval1np = np.power(eval1np, exp)
        scale = .2
    else:
        scale = 1.0/np.nanmax(eval0np)

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')
    for i,j in np.ndindex((TF.size().x,TF.size().y)):
        angle = math.atan2(evec0npx[i,j], evec0npy[i,j])*57.2957
        e = Ellipse(xy=[j,i], width=scale*eval1np[i,j], height=scale*eval0np[i,j], angle=angle)
        ax.add_artist(e)
        e.set_alpha(.5)
    ax.set_xlim(0, TF.size().x-1)
    ax.set_ylim(0, TF.size().y-1)

    plt.xticks([])
    plt.yticks([])
    # plt.axis('tight')
    plt.axis('off')
    # plt.axis('equal')

    plt.gca().invert_yaxis()
    plt.show()
