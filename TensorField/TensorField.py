'''TensorField.py is a base class for TensorField3D and TensorField2D'''

# test  grid = ca.GridInfo(ca.Vec3Di(2,3,4))

import PyCA.Core as ca
import numpy as np
import os


class TensorFieldException(Exception):
    '''Exception class for TensorField'''
    pass


# 2D and 3D tf classes provide self.indices, self.idxdict, self.dim
class TensorField(object):
    '''Base class for TensorField3D and TensorField2D.  You never want
    to actually use this class, because it only contains the shared
    functionality between TensorField3D and TensorField2D'''

    def __setitem__(self, k, v):
        if isinstance(k, (int, long)):
            self.elements[k] = v
        elif len(k) != 2:
            raise TensorFieldException("Improper index - must be a length 2 tuple or int")
        elif k[0] > self.dim-1 or k[0] < 0 or k[1] > self.dim-1 or k[1] < 0:
            raise TensorFieldException("Improper index - tuple out of range!")
        else:
            self.elements[self.idxdict[k]] = v

    def __getitem__(self, k):
        if isinstance(k, (int, long)):
            return self.elements[k]
        elif tuple(k) not in self.indices:
            raise TensorFieldException("Improper index - imvalid tuple")
        return self.elements[self.idxdict[k]]

    def memType(self):          # to match ca.Image3D
        '''returns the memType of all the individual elements (Image3Ds)'''
        return self._memType

    def grid(self):
        '''returns the grid of all the individual elements (Image3Ds)'''
        return self._grid

    def size(self):
        '''returns the size of all the individual elements (Image3Ds)'''
        return self.elements[0].size()

    def toType(self, newmemType):
        for im in self.elements:
            im.toType(newmemType)
        self._memType = newmemType

    def setMem(self, memVal):
        '''Sets all the elements of all the tensors to memVal (usually
        used to zero out a field)'''
        if isinstance(memVal, (int, long, float)):
            for im in self.elements:
                ca.SetMem(im, memVal)

    def setIdentity(self):
        for i, j in self.indices:
            if i == j:
                ca.SetMem(self[i,j], 1.0)
            elif i > j:
                ca.SetMem(self[i,j], 0.0)

    def asnp(self, copy=False):
        '''returns a MxNxPxdimxdim numpy array '''
        nparr_out = np.zeros((self.grid().size().x, self.grid().size().y,
                              self.grid().size().z, self.dim, self.dim))
        for idx in self.indices:
            if copy:
                nparr_out[:,:,:,idx[0],idx[1]] = self[idx].asnp().copy()
            else:
                nparr_out[:,:,:,idx[0],idx[1]] = self[idx].asnp()
        return nparr_out

    def __add__(self, other):
        outTF = self.__class__(self.grid(), self.memType())
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Add(outTF.elements[i], self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.AddC(outTF.elements[i], self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '+'")
        return outTF

    def __sub__(self, other):
        outTF = self.__class__(self.grid(), self.memType())
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Sub(outTF.elements[i], self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.SubC(outTF.elements[i], self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '-'")
        return outTF

    def __mul__(self, other):
        outTF = self.__class__(self.grid(), self.memType())
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Mul(outTF.elements[i], self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.MulC(outTF.elements[i], self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '*'")
        return outTF

    def __div__(self, other):
        outTF = self.__class__(self.grid(), self.memType())
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Div(outTF.elements[i], self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.DivC(outTF.elements[i], self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '/'")
        return outTF


    def __iadd__(self, other):
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Add_I(self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.AddC_I(self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '+='")
        return self

    def __isub__(self, other):
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Sub_I(self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.SubC_I(self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '-='")
        return self

    def __imul__(self, other):
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Mul_I(self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.MulC_I(self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '*='")
        return self

    def __idiv__(self, other):
        for i in xrange(len(self.elements)):
            if isinstance(other, TensorField):
                ca.Div_I(self.elements[i], other.elements[i])
            elif isinstance(other, (int, long, float)):
                ca.DivC_I(self.elements[i], other)
            else:
                raise TensorFieldException("Unknown input type for '/='")
        return self

    def __radd__(self, other):
        return self + other

    def __rmul__(self, other):
        return self * other


def Abs(out, A):
    for i in xrange(len(A.elements)):
        ca.Abs(out[i], A[i])


def Abs_I(A):
    for i in xrange(len(A.elements)):
        ca.Abs_I(A[i])

def Copy(Acpy, A):
    for i in xrange(len(A.elements)):
        ca.Copy(Acpy[i], A[i])

def MinMax(self):
    npMin = np.zeros((self.dim, self.dim))
    npMax = np.zeros((self.dim, self.dim))
    for idx in self.indices:
        [npMin[idx], npMax[idx]] = ca.MinMax(self[idx])
    return [npMin, npMax]

def SetIdentity(A):
    A.setIdentity()
