// crr 2014

// #include <stdio.h>
// #include <algorithm>
// #include <cuda_runtime.h>
#include <math.h>
#include <float.h>

template <typename T> __device__ void inline swap(T& a, T& b)
{
    T c(a); a=b; b=c;
}

__device__ bool inline isvalid(float x )
{
  return (x == x) && (x <= FLT_MAX && x >= -FLT_MAX);
}

extern "C" __global__ void
Inverse3DKernel(float* inv00,
                float* inv01,
                float* inv02,
                float* inv11,
                float* inv12,
                float* inv22,
                const float* M00,
                const float* M01,
                const float* M02,
                const float* M11,
                const float* M12,
                const float* M22,
                unsigned int vNx,
                unsigned int vNy,
                unsigned int vNz){

  unsigned int vx = threadIdx.x;
  unsigned int vy = blockIdx.x;
  unsigned int vz = blockIdx.y;
  if (vx >= vNx || vy >= vNy || vz >= vNz) return; // outside volume
  unsigned int idx = vz*vNx*vNy + vy*vNx + vx;

  float det = M00[idx]*M11[idx]*M22[idx] + M01[idx]*M12[idx]*M02[idx] + M02[idx]*M01[idx]*M12[idx] -
    M02[idx]*M11[idx]*M02[idx] - M01[idx]*M01[idx]*M22[idx] - M00[idx]*M12[idx]*M12[idx];

  inv00[idx] = (M11[idx]*M22[idx] - M12[idx]*M12[idx])/det;
  inv01[idx] = (M02[idx]*M12[idx] - M22[idx]*M01[idx])/det;
  inv02[idx] = (M01[idx]*M12[idx] - M02[idx]*M11[idx])/det;
  inv11[idx] = (M00[idx]*M22[idx] - M02[idx]*M02[idx])/det;
  inv12[idx] = (M02[idx]*M01[idx] - M00[idx]*M12[idx])/det;
  inv22[idx] = (M00[idx]*M11[idx] - M01[idx]*M01[idx])/det;
}

extern "C" __global__ void
Eig2DKernel(float* EVec0x,
              float* EVec0y,
              float* EVec1x,
              float* EVec1y,
              float* EVal0,
              float* EVal1,
            const float* M00,
            const float* M01,
            const float* M11,
            unsigned int vNx,
            unsigned int vNy,
            unsigned int vNz){

  unsigned int vx = threadIdx.x;
  unsigned int vy = blockIdx.x;
  unsigned int vz = blockIdx.y;
  if (vx >= vNx || vy >= vNy || vz >= vNz) return; // outside volume
  unsigned int idx = vz*vNx*vNy + vy*vNx + vx;

  float det, tr;

  // here we have lam0 >= lam 1;
  // if (M01[idx] != 0){
  if(abs(M01[idx]) > 1e-5){
    det = M00[idx]*M11[idx] - M01[idx]*M01[idx];
    tr = M00[idx] + M11[idx];

    EVal0[idx] = (tr + sqrt(tr*tr - 4.0 * det))/2.0;
    EVal1[idx] = (tr - sqrt(tr*tr - 4.0 * det))/2.0;
    // if not diagonal
    // solve for normalized eigenvectors
    float x, y, norm;
    x = M00[idx] - EVal1[idx];
    y = M01[idx];
    norm = sqrt(x*x + y*y);
    EVec0x[idx] = x/norm;
    EVec0y[idx] = y/norm;

    EVec1x[idx] = -EVec0y[idx];
    EVec1y[idx] = EVec0x[idx];

    // if our Vecs/Vals are valid, then return
// this might be unneccssary
    if (isvalid(EVec0x[idx]) && isvalid(EVec0x[idx]) &&
        isvalid(EVec0x[idx]) && isvalid(EVec0x[idx])){
      return;
    }
  }

  // Otherwise we do diagonal or nearly diagonal matrices
  if (M00[idx] >= M11[idx]) { // diagonal
    EVal0[idx] = M00[idx];
    EVal1[idx] = M11[idx];
    EVec0x[idx] = 1;
    EVec0y[idx] = 0;
    EVec1x[idx] = 0;
    EVec1y[idx] = 1;
    return;
  } else {                      // other diagonal
    EVal0[idx] = M11[idx];
    EVal1[idx] = M00[idx];
    EVec0x[idx] = 0;
    EVec0y[idx] = 1;
    EVec1x[idx] = 1;
    EVec1y[idx] = 0;
    return;
  }





}

extern "C" __global__ void
Eig3DKernel(float* EVec0x,
            float* EVec0y,
            float* EVec0z,
            float* EVec1x,
            float* EVec1y,
            float* EVec1z,
            float* EVec2x,
            float* EVec2y,
            float* EVec2z,
            float* EVal0,
            float* EVal1,
            float* EVal2,
            const float* M00,
            const float* M01,
            const float* M02,
            const float* M11,
            const float* M12,
            const float* M22,
            unsigned int vNx,
            unsigned int vNy,
            unsigned int vNz){

  unsigned int vx = threadIdx.x;
  unsigned int vy = blockIdx.x;
  unsigned int vz = blockIdx.y;
  if (vx >= vNx || vy >= vNy || vz >= vNz) return; // outside volume
  unsigned int idx = vz*vNx*vNy + vy*vNx + vx;

  // see wikipedia Eigenvalue_algorithm
  float p1, eig0, eig1, eig2, q, p2, p, r, trM, detB, phi, B00, B01, B02, B11,
    B12, B22;

  trM = M00[idx] + M11[idx] + M22[idx];

  p1 = M01[idx]*M01[idx] + M02[idx]*M02[idx] + M12[idx]*M12[idx];
  if (p1 == 0){
    // M is diagonal.
    eig0 = M00[idx];
    eig1 = M11[idx];
    eig2 = M22[idx];

  } else {
    q = trM/3.0;
    p2 = (M00[idx]-q)*(M00[idx]-q) + (M11[idx]-q)*(M11[idx]-q) +
      (M22[idx]-q)*(M22[idx]-q) + 2*p1;
    p = sqrt(p2 / 6.0);
    // B = (1 / p) * (M - q * I);       // I is the identity matrix
    B00 = (1.0 / p) * (M00[idx] - q);
    B01 = (1.0 / p) * (M01[idx]);
    B02 = (1.0 / p) * (M02[idx]);
    B11 = (1.0 / p) * (M11[idx] - q);
    B12 = (1.0 / p) * (M12[idx]);
    B22 = (1.0 / p) * (M22[idx] - q);
    detB = B00*B11*B22 + B01*B12*B02 + B02*B01*B12 -
      B02*B11*B02 - B01*B01*B22 - B00*B12*B12;
    r = detB / 2.0;

    // In exact arithmetic for a symmetric matrix  -1 <= r <= 1
    // but computation error can leave it slightly outside this range.
    if (r <= -1){
      phi = M_PI / 3;
    } else if (r >= 1) {
      phi = 0;
    } else {
      phi = acos(r) / 3;
    }

    // the eigenvalues satisfy eig2 <= eig1 <= eig0
    eig0 = q + 2 * p * cos(phi);
    eig2 = q + 2 * p * cos(phi + (2*M_PI/3));
    eig1 = trM - eig0 - eig2;   // since trace(M) = eig0 + eig1 + eig2
  }
  // order eigenvalues so that eig2 <= eig1 <= eig0
  if (eig2 > eig1)
    swap(eig2, eig1);
  if (eig2 > eig0)
    swap(eig2, eig0);
  if (eig1 > eig0)
    swap(eig1, eig0);

  EVal0[idx] = eig0;
  EVal1[idx] = eig1;
  EVal2[idx] = eig2;

  if (eig0 == eig1 && eig1 == eig2){ // simple case, a*identity
    EVec0x[idx] = 1.0;
    EVec0y[idx] = 0.0;
    EVec0z[idx] = 0.0;
    EVec1x[idx] = 0.0;
    EVec1y[idx] = 1.0;
    EVec1z[idx] = 0.0;
    EVec2x[idx] = 0.0;
    EVec2y[idx] = 0.0;
    EVec2z[idx] = 1.0;
    return;
  }

  // solve for normalized eigenvectors
  float x, y, z, norm;
  float A00, A01, A11, A02, A12, A22;
  float V0x, V0y, V0z, V1x, V1y, V1z, V2x, V2y, V2z;
  float othereval0, othereval1;

  for (int i=0; i<3; i=i+2){    // skip middle eigenvector
    if (i == 0) {
      othereval0 = EVal1[idx];
      othereval1 = EVal2[idx];
    // } else if (i == 1) {
    //   othereval0 = EVal0[idx];
    //   othereval1 = EVal2[idx];
    } else {                    // i == 2
      othereval0 = EVal0[idx];
      othereval1 = EVal1[idx];
    }
      A00 = M00[idx] - othereval0;
      A01 = M01[idx];
      A02 = M02[idx];
      A11 = M11[idx] - othereval0;
      A12 = M12[idx];
      A22 = M22[idx] - othereval0;

      B00 = M00[idx] - othereval1;
      B01 = M01[idx];
      B02 = M02[idx];
      B11 = M11[idx] - othereval1;
      B12 = M12[idx];
      B22 = M22[idx] - othereval1;

      // choices of (non-normalized) eigenvectors, could include 0,0,0's
      V0x = A00*B00 + A01*B01 + A02*B02;
      V0y = A01*B00 + A11*B01 + A12*B02;
      V0z = A02*B00 + A12*B01 + A22*B02;

      V1x = A00*B01 + A01*B11 + A02*B12;
      V1y = A01*B01 + A11*B11 + A12*B12;
      V1z = A02*B01 + A12*B11 + A22*B12;

      V2x = A00*B02 + A01*B12 + A02*B22;
      V2y = A01*B02 + A11*B12 + A12*B22;
      V2z = A02*B02 + A12*B12 + A22*B22;

      // take the largest mag eigenvector (avoid taking <0,0,0>)
      float norm0sq = V0x*V0x + V0y*V0y + V0z*V0z;
      float norm1sq = V1x*V1x + V1y*V1y + V1z*V1z;
      float norm2sq = V2x*V2x + V2y*V2y + V2z*V2z;
      if(norm0sq >= norm1sq && norm0sq >= norm2sq) {
        x = V0x; y = V0y; z = V0z;
      } else if (norm1sq >= norm0sq && norm1sq >= norm2sq){
        x = V1x; y = V1y; z = V1z;
      } else {
        x = V2x; y = V2y; z = V2z;
      }

      // get norm of vector
      norm = sqrt(x*x + y*y + z*z);

      if (i == 0) {
        EVec0x[idx] = x/norm;
        EVec0y[idx] = y/norm;
        EVec0z[idx] = z/norm;
      } else {                  // i == 2
        EVec2x[idx] = x/norm;
        EVec2y[idx] = y/norm;
        EVec2z[idx] = z/norm;
      }
  }

  float xtemp, ytemp, ztemp;
  // Make sure we don't have NaN values in v0 or v2
  // we should never had NaN values in both
  if (EVec0x[idx] != EVec0x[idx] || // (if NAN)
      EVec0y[idx] != EVec0y[idx] ||
      EVec0z[idx] != EVec0z[idx]){
    // if we have NAN, create a new distinct vector temp
    xtemp = EVec2z[idx];
    ytemp = -EVec2x[idx];
    ztemp = -EVec2y[idx];
    // then cross product it
    x = ytemp*EVec2z[idx] - ztemp*EVec2y[idx];
    y = ztemp*EVec2x[idx] - xtemp*EVec2z[idx];
    z = xtemp*EVec2y[idx] - ytemp*EVec2x[idx];
    // and renormalize
    norm = sqrt(x*x + y*y + z*z);
    EVec0x[idx] = x/norm;
    EVec0y[idx] = y/norm;
    EVec0z[idx] = z/norm;
  }
  // Do the same thing for EVec2
  if (EVec2x[idx] != EVec2x[idx] ||
      EVec2y[idx] != EVec2y[idx] ||
      EVec2z[idx] != EVec2z[idx]){
    xtemp = EVec0z[idx];
    ytemp = -EVec0x[idx];
    ztemp = -EVec0y[idx];
    // then cross product it
    x = ytemp*EVec0z[idx] - ztemp*EVec0y[idx];
    y = ztemp*EVec0x[idx] - xtemp*EVec0z[idx];
    z = xtemp*EVec0y[idx] - ytemp*EVec0x[idx];
    // and renormalize
    norm = sqrt(x*x + y*y + z*z);
    EVec2x[idx] = x/norm;
    EVec2y[idx] = y/norm;
    EVec2z[idx] = z/norm;
  }

  // Middle eigenvector is just EVec0 x EVec2
  x = EVec0y[idx]*EVec2z[idx] - EVec0z[idx]*EVec2y[idx];
  y = EVec0z[idx]*EVec2x[idx] - EVec0x[idx]*EVec2z[idx];
  z = EVec0x[idx]*EVec2y[idx] - EVec0y[idx]*EVec2x[idx];
  // then renormalize for numerical errors
  norm = sqrt(x*x + y*y + z*z);
  EVec1x[idx] = x/norm;
  EVec1y[idx] = y/norm;
  EVec1z[idx] = z/norm;

}
