'''TensorField3D.py supports 3x3 Tensor operations for PyCA'''

from TensorField import TensorField as _TensorField
from TensorField import Abs, Abs_I, Copy, SetIdentity

import PyCA.Core as ca
import numpy as np
import os
import math

try:
    import pycuda.driver as cuda
    import pycuda.autoinit
    from pycuda.compiler import SourceModule
    # Compile and load CUDA file
    _cbdir = os.path.dirname(__file__)
    _f = open(os.path.join(_cbdir, 'TensorKernels.cu'), 'r').read()
    _mod = SourceModule(_f, no_extern_c=True)
    hasPyCUDA = True
except:
    hasPyCUDA = False

class TensorField3DException(Exception):
    '''Exception class for TensorField3D'''
    pass


class TensorField3D(_TensorField):
    '''TensorField3D class for PyCA that represents a field of 3D
    Symmetric Positive Definite Tensors:
    |m00 m01 m02|
    |m01 m11 m12|
    |m02 m12 m22|

    To create an entirely new Tensor, call
    TF = TensorField3D(grid, memType)

    To create a Tensor off of a list of 6 Image3Ds (with the same grid/memtype)
    TF = TensorField3D(Imlist=[Im00, Im01, Im02, Im11, Im12, Im22])
    '''

    def __init__(self, grid=None, memType=None, Imlist=None):
        if Imlist is None and grid is not None and memType is not None:
            # self.elements = tuple((ca.Image3D(grid, memType) for _ in xrange(6)))
            self.elements = [ca.Image3D(grid, memType) for _ in xrange(6)]
            assert(isinstance(grid, ca.GridInfo))
            assert(isinstance(memType, int))
            self._grid = grid   # hidden so that we can make .grid a method
            self._memType = memType
        elif Imlist is not None:
            if len(Imlist) != 6:
                raise TensorField3DException("'elements' should be length 6")
            # self.elements = tuple(Imlist)
            self.elements = Imlist
            self._grid = self.elements[0].grid()
            self._memType = self.elements[0].memType()
        else:
            raise TensorField3DException("bad initialization: needs grid+memType or Imtuple")

        self.indices = tuple(((0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), (2,1), (2,2)))
        # dict that converst between the indices (int vs tuples)
        self.idxdict = {0:(0,0), 1:(0,1), 2:(0,2), 3:(1,1), 4:(1,2), 5:(2,2),
                        (0,0):0, (0,1):1, (0,2):2,
                        (1,0):1, (1,1):3, (1,2):4,
                        (2,0):2, (2,1):4, (2,2):5}
        self.dim = 3


    # def __add__(self, other):
    #     # outTF = TensorField3D(self.grid(), self.memType())
    #     outTF = self.__class__(self.grid(), self.memType())
    #     for i in xrange(6):
    #         if isinstance(other, TensorField3D):
    #             ca.Add(outTF.elements[i], self.elements[i], other.elements[i])
    #         elif isinstance(other, (int, long, float)):
    #             ca.AddC(outTF.elements[i], self.elements[i], other)
    #         else:
    #             raise TensorField3DException("Unknown input type for '+'")
    #     return outTF

    # def __sub__(self, other):
    #     outTF = TensorField3D(self.grid(), self.memType())
    #     for i in xrange(6):
    #         if isinstance(other, TensorField3D):
    #             ca.Sub(outTF.elements[i], self.elements[i], other.elements[i])
    #         elif isinstance(other, (int, long, float)):
    #             ca.SubC(outTF.elements[i], self.elements[i], other)
    #         else:
    #             raise TensorField3DException("Unknown input type for '-'")
    #     return outTF

    # def __mul__(self, other):
    #     outTF = TensorField3D(self.grid(), self.memType())
    #     for i in xrange(6):
    #         if isinstance(other, TensorField3D):
    #             ca.Mul(outTF.elements[i], self.elements[i], other.elements[i])
    #         elif isinstance(other, (int, long, float)):
    #             ca.MulC(outTF.elements[i], self.elements[i], other)
    #         else:
    #             raise TensorField3DException("Unknown input type for '*'")
    #     return outTF

    # def __div__(self, other):
    #     outTF = TensorField3D(self.grid(), self.memType())
    #     for i in xrange(6):
    #         if isinstance(other, TensorField3D):
    #             ca.Div(outTF.elements[i], self.elements[i], other.elements[i])
    #         elif isinstance(other, (int, long, float)):
    #             ca.DivC(outTF.elements[i], self.elements[i], other)
    #         else:
    #             raise TensorField3DException("Unknown input type for '/'")
    #     return outTF



def Dot(out, A, B):
    if isinstance(A, TensorField3D) and isinstance(B, TensorField3D):
        for i,j in A.indices:
            ca.Mul(out[i,j], A[i,0], B[0,j])
            ca.Add_Mul_I(out[i,j], A[i,1], B[1,j])
            ca.Add_Mul_I(out[i,j], A[i,2], B[2,j])
    elif isinstance(A, TensorField3D) and isinstance(B, ca.Field3D):
        M, v = A, B
        Out_idx = ca.Image3D(M.grid(), M.memType())
        v_idx = ca.Image3D(M.grid(), M.memType())
        for i in xrange(3):
            ca.Copy(v_idx, v, 0)
            ca.Mul(Out_idx, M[i,0], v_idx)
            ca.Copy(v_idx, v, 1)
            ca.Add_Mul_I(Out_idx, M[i,1], v_idx)
            ca.Copy(v_idx, v, 2)
            ca.Add_Mul_I(Out_idx, M[i,2], v_idx)
            ca.Copy(out, Out_idx, i) # copy result back to 'out'
    elif isinstance(A, ca.Field3D) and isinstance(B, TensorField3D):
        Dot(out, B, A)
    elif isinstance(A, ca.Field3D) and isinstance(B, ca.Field3D):
        Aidx = ca.Image3D(A.grid(), A.memType())
        Bidx = ca.Image3D(A.grid(), A.memType())
        ca.Copy(Aidx, A, 0)
        ca.Copy(Bidx, B, 0)
        ca.Mul(out, Aidx, Bidx)
        ca.Copy(Aidx, A, 1)
        ca.Copy(Bidx, B, 1)
        ca.Add_Mul_I(out, Aidx, Bidx)
        ca.Copy(Aidx, A, 2)
        ca.Copy(Bidx, B, 2)
        ca.Add_Mul_I(out, Aidx, Bidx)
    else:
        raise TensorField3DException("Unknown input types for Dot")


def Det(Imdet, M, is2D=False):
    '''Gives the determinant of a TensorField3D `M`.  The determinant is
    stored as an Image3D `Imdet`'''
    Imtmp = ca.Image3D(M.grid(), M.memType())
    ca.MulMul(Imdet, M[0,0], M[1,1], M[2,2])
    ca.MulMul(Imtmp, M[0,1], M[1,2], M[2,0])
    Imdet += Imtmp
    ca.MulMul(Imtmp, M[0,2], M[1,0], M[2,1])
    Imdet += Imtmp
    ca.MulMul(Imtmp, M[0,2], M[1,1], M[2,0])
    Imdet -= Imtmp
    ca.MulMul(Imtmp, M[0,1], M[1,0], M[2,2])
    Imdet -= Imtmp
    ca.MulMul(Imtmp, M[0,0], M[1,2], M[2,1])
    Imdet -= Imtmp


def Inverse(TFinv, TF):
    '''compute the Matrix Inverse for each tensor in the tensor field'''
    if TFinv.memType() != TF.memType():
        raise TensorField3DException('Different memType!')
    if TF.memType() == ca.MEM_HOST:
        raise TensorField3DException('Not Yet Implemented')
    else:
        Imblock = (TF.size().x, 1, 1)
        Imgrid = (TF.size().y, TF.size().z, 1)
        Inverse3DKernel = _mod.get_function("Inverse3DKernel")

        Inverse3DKernel(np.uint64(TFinv[0,0].rawptr()),
                        np.uint64(TFinv[0,1].rawptr()),
                        np.uint64(TFinv[0,2].rawptr()),
                        np.uint64(TFinv[1,1].rawptr()),
                        np.uint64(TFinv[1,2].rawptr()),
                        np.uint64(TFinv[2,2].rawptr()),
                        np.uint64(TF[0,0].rawptr()),
                        np.uint64(TF[0,1].rawptr()),
                        np.uint64(TF[0,2].rawptr()),
                        np.uint64(TF[1,1].rawptr()),
                        np.uint64(TF[1,2].rawptr()),
                        np.uint64(TF[2,2].rawptr()),
                        np.uint32(TF.size().x),
                        np.uint32(TF.size().y),
                        np.uint32(TF.size().z),
                        block=Imblock,
                        grid=Imgrid)


# def MinMax(A):
#     return[[ca.MinMax(A[0,0]), ca.MinMax(A[0,1]), ca.MinMax(A[0,2])],
#            [ca.MinMax(A[1,0]), ca.MinMax(A[1,1]), ca.MinMax(A[1,2])],
#            [ca.MinMax(A[2,0]), ca.MinMax(A[2,1]), ca.MinMax(A[2,2])]]


def Eig(EVec0, EVec1, EVec2, EVal0, EVal1, EVal2, TF):
    '''Computes Eig of a Tensor2D, putting eigenvectors in a list of 2
    Field3Ds and corresponding eigenvalues in a list of 2 Image3Ds.
    The eigenvalues are ordered from smallest to largest
    '''
    # TODO: check mtypes of everything
    if TF._memType == ca.MEM_HOST:
        # Directly translated from the CUDA kernel
        it = np.nditer(EVal0.asnp(), flags=['multi_index'], op_flags=['writeonly'])
        while not it.finished:
            idx = it.multi_index

            # get tensor components
            M00 = TF[0,0].get(idx[0], idx[1], idx[2])
            M01 = TF[0,1].get(idx[0], idx[1], idx[2])
            M02 = TF[0,2].get(idx[0], idx[1], idx[2])
            M11 = TF[1,1].get(idx[0], idx[1], idx[2])
            M12 = TF[1,2].get(idx[0], idx[1], idx[2])
            M22 = TF[2,2].get(idx[0], idx[1], idx[2])

            # compute trace
            trM = M00 + M11 + M22

            # squared norm of off-diagonal entries
            p1 = M01**2 + M02**2 + M12**2

            if p1 == 0:  # M is diagonal
                # TODO: Shouldn't this be a floating point compare?
                eig0 = M00
                eig1 = M11
                eig2 = M22
            else:
                q = trM / 3.0
                p2 = (M00-q)**2 + (M11-q)**2 + (M22-q)**2 + 2*p1
                p = np.sqrt(p2 / 6.0)

                # B = (1/p) * (M - q*I)
                B00 = (1.0/p) * (M00 - q)
                B01 = (1.0/p) * (M01)
                B02 = (1.0/p) * (M02)
                B11 = (1.0/p) * (M11 - q)
                B12 = (1.0/p) * (M12)
                B22 = (1.0/p) * (M22 - q)

                detB = B00*B11*B22 + B01*B12*B02 + B02*B01*B12 - \
                  B02*B11*B02 - B01*B01*B22 - B00*B12*B12
                r = detB * 0.5

                # In exact arithmetic for a symmetric matridx  -1 <= r <= 1
                # but computation error can leave it slightly outside this range.
                if r <= -1.0:
                    phi = np.pi / 3.0
                elif r >= 1.0:
                    phi = 0.0
                else:
                    phi = math.acos(r) / 3.0

                # the eigenvalues satisfy eig2 <= eig1 <= eig0
                eig0 = q + 2.0 * p * np.cos(phi)
                eig2 = q + 2.0 * p * np.cos(phi + (2.0*np.pi/3.0))
                eig1 = trM - eig0 - eig2  # since trace(M) = eig0 + eig1 + eig2

            # order eigenvalues so that eig2 <= eig1 <= eig0
            if (eig2 > eig1):
                t = eig2
                eig2 = eig1
                eig1 = t
            if (eig2 > eig0):
                t = eig2
                eig2 = eig0
                eig0 = t
            if (eig1 > eig0):
                t = eig1
                eig1 = eig0
                eig0 = t

            # Now we have ordered eigenvalues, set them in output array
            EVal0.set(idx[0], idx[1], idx[2], eig0)
            EVal1.set(idx[0], idx[1], idx[2], eig1)
            EVal2.set(idx[0], idx[1], idx[2], eig2)

            if eig0 == eig1 and eig1 == eig2:  # simplace case, a*identity
                EVec0.set(idx[0], idx[1], idx[2], ca.Vec3Df(0,0,0))
                EVec1.set(idx[0], idx[1], idx[2], ca.Vec3Df(0,0,0))
                EVec2.set(idx[0], idx[1], idx[2], ca.Vec3Df(0,0,0))
                continue

            # solve for normalized eigenvectors
            for i in [0,2]:
                if i == 0:
                    othereval0 = EVal1.get(idx[0], idx[1], idx[2])
                    othereval1 = EVal2.get(idx[0], idx[1], idx[2])
                else:
                    othereval0 = EVal0.get(idx[0], idx[1], idx[2])
                    othereval1 = EVal1.get(idx[0], idx[1], idx[2])

                A00 = M00 - othereval0
                A01 = M01
                A02 = M02
                A11 = M11 - othereval0
                A12 = M12
                A22 = M22 - othereval0

                B00 = M00 - othereval1
                B01 = M01
                B02 = M02
                B11 = M11 - othereval1
                B12 = M12
                B22 = M22 - othereval1

                # choices of (non-normalized) eigenvectors, could include 0,0,0's
                V0x = A00*B00 + A01*B01 + A02*B02
                V0y = A01*B00 + A11*B01 + A12*B02
                V0z = A02*B00 + A12*B01 + A22*B02

                V1x = A00*B01 + A01*B11 + A02*B12
                V1y = A01*B01 + A11*B11 + A12*B12
                V1z = A02*B01 + A12*B11 + A22*B12

                V2x = A00*B02 + A01*B12 + A02*B22
                V2y = A01*B02 + A11*B12 + A12*B22
                V2z = A02*B02 + A12*B12 + A22*B22

                # take the largest mag eigenvector (avoid taking <0,0,0>)
                norm0sq = V0x**2 + V0y**2 + V0z**2
                norm1sq = V1x**2 + V1y**2 + V1z**2
                norm2sq = V2x**2 + V2y**2 + V2z**2

                if norm0sq >= norm1sq and norm0sq >= norm2sq:
                    xyz = ca.Vec3Df(V0x, V0y, V0z)
                elif norm1sq >= norm0sq and norm1sq >= norm2sq:
                    xyz = ca.Vec3Df(V1x, V1y, V1z)
                else:
                    xyz = ca.Vec3Df(V2x, V2y, V2z)

                # get norm of vector
                norm = xyz.normL2()

                if i == 0:
                    EVec0.set(idx[0], idx[1], idx[2], xyz / norm)
                else:
                    EVec2.set(idx[0], idx[1], idx[2], xyz / norm)

            EVec0idx = EVec0.get(idx[0], idx[1], idx[2])
            EVec1idx = EVec1.get(idx[0], idx[1], idx[2])
            EVec2idx = EVec2.get(idx[0], idx[1], idx[2])

            # Make sure we don't have NaN values in v0 or v2
            # we should never had NaN values in both
            if EVec0idx.x != EVec0idx.x or \
              EVec0idx.y != EVec0idx.y or \
              EVec0idx.z != EVec0idx.z:
                # if we have NAN, create a new distinct vector temp
                xtemp = EVec2idx.z
                ytemp = -EVec2idx.x
                ztemp = -EVec2idx.y
                # then cross product it
                x = ytemp*EVec2idx.z - ztemp*EVec2idx.y
                y = ztemp*EVec2idx.x - xtemp*EVec2idx.z
                z = xtemp*EVec2idx.y - ytemp*EVec2idx.x
                # and renormalize
                norm = np.sqrt(x**2 + y**2 + z**2)
                EVec0.set(idx[0], idx[1], idx[2], ca.Vec3Df(x, y, z)/norm)
            # Do the same thing for EVec2
            if EVec2idx.x != EVec2idx.x or \
              EVec2idx.y != EVec2idx.y or \
              EVec2idx.z != EVec2idx.z:
                xtemp = EVec0idx.z
                ytemp = -EVec0idx.x
                ztemp = -EVec0idx.y
                # then cross product it
                x = ytemp*EVec0idx.z - ztemp*EVec0idx.y
                y = ztemp*EVec0idx.x - xtemp*EVec0idx.z
                z = xtemp*EVec0idx.y - ytemp*EVec0idx.x
                # and renormalize
                norm = np.sqrt(x**2 + y**2 + z**2)
                EVec2idx = ca.Vec3Df(x, y, z)/norm

            # Middle eigenvector is just EVec0 x EVec2
            xyz = EVec0idx.cross(EVec2idx)
            # then renormalize for numerical errors
            EVec1.set(idx[0], idx[1], idx[2], xyz / xyz.normL2())

            it.iternext()

    elif TF._memType == ca.MEM_DEVICE:
        if not hasPyCUDA:
            raise Exception("Attempted to use CUDA but PyCUDA could not be loaded")

        Imblock = (TF.size().x, 1, 1)
        Imgrid = (TF.size().y, TF.size().z, 1)
        Eig3DKernel = _mod.get_function("Eig3DKernel")

        ca.SetToZero(EVec0)
        ca.SetToZero(EVec1)

        Eig3DKernel(np.uint64(EVec0.rawptr_x()),
                    np.uint64(EVec0.rawptr_y()),
                    np.uint64(EVec0.rawptr_z()),
                    np.uint64(EVec1.rawptr_x()),
                    np.uint64(EVec1.rawptr_y()),
                    np.uint64(EVec1.rawptr_z()),
                    np.uint64(EVec2.rawptr_x()),
                    np.uint64(EVec2.rawptr_y()),
                    np.uint64(EVec2.rawptr_z()),
                    np.uint64(EVal0.rawptr()),
                    np.uint64(EVal1.rawptr()),
                    np.uint64(EVal2.rawptr()),
                    np.uint64(TF[0,0].rawptr()),
                    np.uint64(TF[0,1].rawptr()),
                    np.uint64(TF[0,2].rawptr()),
                    np.uint64(TF[1,1].rawptr()),
                    np.uint64(TF[1,2].rawptr()),
                    np.uint64(TF[2,2].rawptr()),
                    np.uint32(TF.size().x),
                    np.uint32(TF.size().y),
                    np.uint32(TF.size().z),
                    block=Imblock,
                    grid=Imgrid)
    else:
        raise Exception('Unknown memtype {}'.format(TF._memType))
